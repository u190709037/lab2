public class FindGrade {
    public static void main(String[] args) {
        int Score = Integer.parseInt(args[0]);

        if ((Score<=100)&&(Score>=90)) {
            System.out.println("Your grade is A"); }
        else if ((Score<90)&&(Score>=80)) {
            System.out.println("Your grade is B"); }
        else if ((Score<80)&&(Score>=70)) {
            System.out.println("Your grade is C"); }
        else if ((Score<70)&&(Score>=60)) {
            System.out.println("Your grade is D"); }
        else if ((Score<60)&&(Score>=0)) {
            System.out.println("Your grade is F"); }
        else {
            System.out.println("It is not a valid score!"); }

    }

}
