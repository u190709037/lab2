public class FindMin {

    public static void main(String[] args) {
        int Value1=Integer.parseInt(args[0]);
        int Value2=Integer.parseInt(args[1]);
        int Value3=Integer.parseInt(args[2]);
        int minValue;

        boolean Condition1 = Value1 <= Value2;
        minValue=Condition1 ? Value1 : Value2;
        boolean Condition2 = minValue <= Value3;
        minValue= Condition2 ? minValue : Value3;

        System.out.println(minValue);


    }
}
